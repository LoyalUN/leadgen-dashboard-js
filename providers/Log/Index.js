'use strict'

// Declare Dependencies
const moment  = require('moment');
require('winston-daily-rotate-file');
const { createLogger, format, transports } = require('winston');
const { combine, printf } = format;

const myFormat = printf(info => {
  return `${moment().format('YYYY-MM-DD HH:mm:ss')} : ${info.level} : ${info.message}`;
});


class Log {

    constructor (Config) {

        const settings = Config.get(`log.settings`);
        if (!settings) {
            throw new Error (`Specify configs under config/log file`);
        }
        
        
        var daily_transport = new (transports.DailyRotateFile)({
            filename: `${settings.filename}-%DATE%.log`,
            datePattern: 'YYYY-MM-DD-HH',
            zippedArchive: true,
            maxFiles: '14d'
        });
        
        const logger = createLogger({
            level: settings.info,
            format: combine(
                myFormat
            ),
            transports: [
                new transports.Console(),
                daily_transport
            ]
        });


        return logger;
    }

}

module.exports = Log
