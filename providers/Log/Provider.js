const { ServiceProvider } = require('@adonisjs/fold')

class LogProvider extends ServiceProvider {
  register () {
    this.app.singleton('OMGLeadGenDashboard/Log', () => {
      const Config = this.app.use('Adonis/Src/Config')
      const Log = require('./Index')
      return new Log(Config)
    })
  }
}

module.exports = LogProvider
