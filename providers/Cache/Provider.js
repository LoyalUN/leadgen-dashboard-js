const { ServiceProvider } = require('@adonisjs/fold')

class CacheProvider extends ServiceProvider {
  register () {
    this.app.singleton('OMGLeadGenDashboard/Cache', () => {
      const Config = this.app.use('Adonis/Src/Config')
      const Cache = require('./Index')
      return new Cache(Config)
    })
  }
}

module.exports = CacheProvider
