'use strict'

// Declare Dependencies
const log = use('Logger');
const redis = require('then-redis');

// Init Class
class Cache {

  constructor (Config) {
    const driver = Config.get('cache.driver')
    if (!driver) {
      throw new Error(`Specify driver under config/cache file`)
    }

    const connectionSettings = Config.get(`cache.${driver}`)
    if (!connectionSettings) {
      throw new Error(`Specify ${driver} under config/cache file`)
    }

    return this.connect(connectionSettings.connection);
  }

  connect(connectionSettings){
    try {
  
      // default to redis for now
      const instance = redis.createClient({
        host: connectionSettings.host,
        port: connectionSettings.port,
        password: connectionSettings.password,
        database: connectionSettings.database,
      })

      instance.on('ready', function() {
        log.info(`[Cache Provider] :: Ready`);
      });

      instance.on('connect', function() {
        log.info(`[Cache Provider] :: Connected`);
        return instance;
      });

      instance.on('end', function() {
        log.error(`[Cache Provider] :: Connection Ended`);
      });

      return instance;
      
    } catch(ex) {
      log.error(`[Cache Provider] :: ${ex.message}`);
    }
  }

}

module.exports = Cache
