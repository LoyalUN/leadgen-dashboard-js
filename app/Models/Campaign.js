'use strict'

const Model = use('Model')

class Campaign extends Model {
    static get table () {
        return 'campaigns'
    }
}

module.exports = Campaign
