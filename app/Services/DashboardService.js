'use strict';

// Declare Dependencies
const log = use('OMGLeadGenDashboard/Log');
const Campaign = use('App/Models/Campaign');



// Init Class
class DashboardService {


    async getAllCampaigns(){
        log.info(`QuotationService :: getAllCampaigns`);
 
        try {

            let campaign = await Campaign.all();
            return campaign;

        } catch (ex) {
            log.error(`QuotationService :: getAllCampaigns :: ${ex.message}`);
            return this.handleException(ex, response);
        }
    }
    

    async updateCampaign(campaign_id, name, code, keyword, rate, sender_id, doi, doi_message,response, response_message, provider_id){
        log.info(`QuotationService :: updateCampaign`);
 
        try {

            let campaign = await Campaign.find(campaign_id);
            campaign.name = name;
            campaign.code = code;
            campaign.keyword = keyword;
            campaign.rate = rate;
            campaign.sender_id = sender_id;
            campaign.doi = doi;
            campaign.response = response;
            campaign.doi_message = doi_message;
            campaign.response_message = response_message;
            campaign.provider_id = provider_id;
            campaign.save()
            return campaign;

        } catch (ex) {
            log.error(`QuotationService :: updateCampaign :: ${ex.message}`);
            return this.handleException(ex, response);
        }
    }
}

module.exports = new DashboardService;
