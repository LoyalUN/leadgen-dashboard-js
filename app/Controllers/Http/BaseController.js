'use strict'

// Declare Dependency
const rp = require('request-promise');
const log = use('OMGLeadGenDashboard/Log');

class BaseController {


  fetch (options) {
    let default_options = {
        timeout: 30000,
        json: true,
    };
    let request_options = Object.assign(default_options, options);
    try {
        return new Promise((resolve, reject) => {
            rp(request_options).then((response) => {
                log.debug(`Request to ${request_options.uri}`);
                resolve(response);
            }).catch((err) => {
                log.error(`Fetch error for: ${JSON.stringify(request_options)}`, err.message);
                reject(err);
            });
        });
    } catch (ex) {
        log.error(`Fetch error for: ${JSON.stringify(request_options)}`, err.message);
        return null;
    }
  }



  respond(response, data){
    log.debug(`BaseCtrl :: Response ${JSON.stringify(data.code)} :: ${JSON.stringify(data.message)}`);
    
    return response.status(data.code).send(data.message);
  }

  renderView(view, screen, data, request){
    log.debug(`BaseCtrl :: Render View :: Screen :: ${screen} :: Data ::${JSON.stringify(data)}`);
    console.log('user data', request.user);
    let global_data = {
      user: request.user
    };

    let screen_data = Object.assign(global_data, data);
    return view.render(screen, screen_data);
  }

  base64encode(str){
    return new Buffer(str).toString('base64')
  }

  base64decode(str){
    return (new Buffer(str, 'base64')).toString('utf8'); 
  }

  check(req, parameter_name){
    if(!req.body[parameter_name]) {
      let exception = {
        message: "Missing parameter: " + parameter_name,
        code: 400
      }
      throw exception;
    } else {
      return req.body[parameter_name];
    }
  }

  checkParams(params, parameter_name){
    if(!params[parameter_name]) {
      let exception = {
        message: "Missing parameter: " + parameter_name,
        code: 400
      }
      throw exception;
    } else {
      return params[parameter_name];
    }
  }

  checkHeader(req, parameter_name){
    if(!req.header(parameter_name.toLowerCase())) {
      let exception = {
        code: 400,
        message: "Header Missing: " + parameter_name,        
      }
      throw exception;
    } else {
      return req.header(parameter_name.toLowerCase());
    }
  }

  checkQuery(request, parameter_name){
    if(!request.input(parameter_name)) {
      let exception = {
        message: "Missing Paramater: " + parameter_name,
        code: 400
      }
      throw exception;
    } else {
      return request.input(parameter_name);
    }
  }

  _500InternalServiceError(){
    return {
        errors: [
          {
            success: false,
            text: "Internal Server Error"
          }
        ]
      }        
  }
  
  handleException(ex, res){
    log.error(`HandleException :: API Error :: ${JSON.stringify(ex.message)}`);
    if (!isNaN(ex.code)){
      return res.status(ex.code).send(ex.message);
    } else {
      return res.status(500).send(this._500InternalServiceError());
    }
  }

  async handleViewException (ex, response, session) {
    log.error(`HandleException :: View Error :: ${JSON.stringify(ex.message)}`);

    session
    .withErrors([{ field: 'error', message: ex.message }])
    .flashAll()
    await session.commit();
    return response.redirect('back');
  }
  
}

module.exports = BaseController
