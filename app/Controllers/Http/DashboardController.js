'use strict'


const Env = use('Env');
const log = use('OMGLeadGenDashboard/Log');
const BaseController = require('./BaseController');
const DashboardService = use('App/Services/DashboardService')

const app_url = Env.get("APP_URL")

class DashboardController extends BaseController {
    async index({view}){
        log.info('DashboardController')
        return view.render('dashboard.index', {'app_url':app_url});
    }
    async getAllCampaigns ({ request, response, params }){
        log.info('DashboardController :: getAllCampaigns');
  
        try {
  
          let campaigns = await DashboardService.getAllCampaigns()

          let data = {
            code: 200,
            message: campaigns
          }
  
          return this.respond(response, data);
        } catch (ex) {
            log.error(`DashboardController :: getAllCampaigns :: ${ex.message}`);
            return this.handleException(ex, response);
        }
    }
    async updateCampaign ({ request, response, params }){
        log.info('DashboardController :: updateCampaign');

        let requestData = request.post()
        // let requestDataObj = requestData[0]
        console.log(requestData.id)
        let campaign_id = requestData.id
        let name = requestData.name
        let code = requestData.code
        let keyword = requestData.keyword
        let rate = requestData.rate
        let sender_id = requestData.sender_id 
        let doi = requestData.doi
        let doi_message = requestData.doi_message
        let given_response = requestData.response
        let response_message = requestData.response
        let provider_id = requestData.provider_id
        try {
  
          let campaigns = await DashboardService.updateCampaign(campaign_id, name, code, keyword, rate, sender_id, doi, doi_message,given_response, response_message, provider_id)

          let data = {
            code: 200,
            message: campaigns
          }
  
          return this.respond(response, data);
        } catch (ex) {
            log.error(`DashboardController :: updateCampaign :: ${ex.message}`);
            return this.handleException(ex, response);
        }
    }
}

module.exports = DashboardController
