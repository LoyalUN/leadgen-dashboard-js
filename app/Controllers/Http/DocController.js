'use strict'

const log = use('OMGLeadGenDashboard/Log');

class DocController {
    async docs({view}){
        log.info('docs')
        return view.render('docs');
    }
}

module.exports = DocController
