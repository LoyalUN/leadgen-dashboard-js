'use strict'

const Env = use('Env')

const app_name = Env.get('APP_NAME');

module.exports = {

  settings: {
    level: Env.get('LOG_LEVEL', 'info'),
    filename: app_name ? `/var/log/${Env.get('APP_NAME')}/application` : `/var/log/adonisjs_${Env.get('PORT')}/application`
  }

}
